package rbers.solr.utilities;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import rbers.common.Constants;
import rbers.solr.RbersConstants;
import rbers.solr.datatypes.Entity;
import rbers.solr.datatypes.RelatedEntityRelClickCount;
import rbers.solr.datatypes.Relationship;

/**
 * Utility class which extracts various level of details from retrieved solr
 * documents
 */
public class Utility {

	/**
	 * This generates an entity object from the retrieved solrDoc. Here, the
	 * searching is done on the basis of URI so it is assumed that per URI, only
	 * 1 doc exists in knowledge base
	 *
	 * @param docs
	 *            - Retrieved solr doc for the queried URI
	 * @return an Entity object
	 * @throws Exception
	 */
	public static Entity getEntity(SolrDocumentList docs) throws Exception {

		// URI is unique for all entities
		if (docs != null && docs.size() == 1) {
			SolrDocument solrDoc = docs.get(0);
			Collection<String> fieldNames = solrDoc.getFieldNames();
			String entityUri = (String) solrDoc
					.getFieldValue(RbersConstants.URI);

			Entity queriedEntity = new Entity(entityUri);

			for (String field : fieldNames) {
				// searching for all relationships(including _resource shows a
				// relationship in knowledge base) for the queried entity
				if (field.contains("_resource") == false) {
					continue;
				}

				// allValuesOfField shows all the related entities uri
				// corresponding to a relationship
				List<String> allValuesOfField = null;
				Object fieldValue = solrDoc.getFieldValue(field);

				if (fieldValue instanceof String) {
					allValuesOfField = Arrays.asList((String) fieldValue);
				} else if (fieldValue instanceof ArrayList) {
					allValuesOfField = (ArrayList<String>) fieldValue;
				}

				Relationship relationship = new Relationship(field);
				List<Entity> relatedEntities = new ArrayList<>();

				for (String fieldValueString : allValuesOfField) {
					Entity relatedEntity = new Entity(fieldValueString);
					relatedEntities.add(relatedEntity);
				}
				queriedEntity.addRelationshipAndEntity(relationship,
						relatedEntities);
			}
			return queriedEntity;
		} else {
			if (docs == null || docs.size() == 0) {
				throw new Exception("Found 0 Documents");
			} else {
				throw new Exception("Found " + docs.size()
						+ " Documents for given URI");
			}
		}
	}

	/**
	 * This method creates a solr format relationship query for user profile
	 * search, from the given queried entity object
	 *
	 * Format: relationship:rel1 OR relationship:rel2 OR relationship:rel3 OR...
	 *
	 * @param queriedEntity
	 *            - entity to be searched in solr
	 * @return String of created query
	 */
	public static String createQueryFromEntity(Entity queriedEntity) {
		StringBuffer relationshipStringForQuery = new StringBuffer("");
		int i = 0, totalRelationshipsInQueriedEntity = queriedEntity
				.getRelationshipAndEntityMap().keySet().size();

		for (Relationship entityRel : queriedEntity
				.getRelationshipAndEntityMap().keySet()) {
			relationshipStringForQuery.append(Constants.SOLR_RELATIONSHIP)
					.append(":").append(entityRel.getName());

			// Do not append "OR" after the last relationship
			if (i < totalRelationshipsInQueriedEntity - 1) {
				relationshipStringForQuery.append(" OR ");
			}
			i++;
		}
		return relationshipStringForQuery.toString();
	}

	/**
	 * This method takes input of all the retrieved matched documents and
	 * extracts all the relationships from those docs
	 *
	 * @param docs
	 *            - retrieved matched solr docs
	 * @return list of relationships
	 */
	public static List<Relationship> getOrderedRel(SolrDocumentList docs) {
		List<Relationship> relationships = new ArrayList<>();

		// URI is unique for all entities
		if (docs != null && docs.size() != 0) {
			for (SolrDocument solrDoc : docs) {
				Collection<String> fieldNames = solrDoc.getFieldNames();
				String relationshipName = null;
				for (String fieldName : fieldNames) {
					if (fieldName.contains(RbersConstants.RELATIONSHIP)) {
						relationshipName = (String) solrDoc
								.getFieldValue(fieldName);
					}
				}
				relationships.add(new Relationship(relationshipName));
			}
		}
		return relationships;
	}

	/**
	 * This method extracts and updates all the details of the related entities
	 * in a map
	 *
	 * @param docs
	 *            - related entities docs for matched relationships
	 * @param mapOfUriAndEntity
	 *            - Map containing URI as key and corresponding Entity as value
	 * @throws Exception
	 */
	public static void updateEntityInMapForEachDoc(SolrDocumentList docs,
			Map<String, Entity> mapOfUriAndEntity) throws Exception {

		// URI is unique for all entities
		if (docs != null) {
			for (SolrDocument solrDoc : docs) {
				Collection<String> fieldNames = solrDoc.getFieldNames();
				String entityUri = (String) solrDoc
						.getFieldValue(RbersConstants.URI);

				// Here, queriedEntity refers to related entity
				Entity queriedEntity = mapOfUriAndEntity.get(entityUri);

				for (String field : fieldNames) {
					if (field.contains("_resource") == false) {
						continue;
					}

					List<String> allValuesOfField = null;
					Object fieldValue = solrDoc.getFieldValue(field);
					if (fieldValue instanceof String) {
						allValuesOfField = Arrays.asList((String) fieldValue);
					} else if (fieldValue instanceof ArrayList) {
						allValuesOfField = (ArrayList<String>) fieldValue;
					}
					Relationship relationship = new Relationship(field);
					List<Entity> relatedEntities = new ArrayList<>();
					for (String fieldValueString : allValuesOfField) {
						Entity relatedEntity = new Entity(fieldValueString);
						relatedEntities.add(relatedEntity);
					}
					queriedEntity.addRelationshipAndEntity(relationship,
							relatedEntities);
				}
			}
		} else {
			if (docs == null || docs.size() == 0) {
				throw new Exception("Found 0 Documents");
			}
		}
	}

	/**
	 * This sums up the click count of all the relationships found in the input
	 * matched docs, for the given entity
	 *
	 * @param docs
	 *            - matched solr docs
	 * @param entity
	 *            - Related entity for which the total click count has to be
	 *            calculate
	 * @return an object of RelatedEntityRelClickCount
	 */
	public static RelatedEntityRelClickCount calculateRelatedEntityRelClickCountTotal(
			SolrDocumentList docs, Entity entity) {
		long total = 0;

		for (SolrDocument doc : docs) {
			long relClickCount = (Long) doc
					.getFieldValue(RbersConstants.CLICK_COUNT);
			total += relClickCount;
		}

		RelatedEntityRelClickCount r = new RelatedEntityRelClickCount(
				entity.getUniqueIdentifier(), total);

		return r;
	}
}
