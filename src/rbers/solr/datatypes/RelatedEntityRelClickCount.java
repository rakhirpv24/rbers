package rbers.solr.datatypes;

/**
 * This class stores the URI of the related entity and the total click count of
 * all those relationships of that related entity which matches user profile
 * relationships for that user
 */
public class RelatedEntityRelClickCount {
	private String uri;
	private long totalRelCount;

	/*
	 * Constructor
	 */
	public RelatedEntityRelClickCount(String uri, long totalRelCount) {
		this.uri = uri;
		this.totalRelCount = totalRelCount;
	}

	/**
	 * Getter for URI of related entity
	 *
	 * @return uri
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * Setter for URI of related entity
	 *
	 * @param uri
	 *            - URI of related entity
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	/**
	 * Getter for total matched(with user profile of that user) relationships
	 * click count of the related entity
	 *
	 * @return total click count
	 */
	public long getTotalRelCount() {
		return totalRelCount;
	}

	/**
	 * Setter for total matched(with user profile of that user) relationships
	 * click count of the related entity
	 *
	 * @param totalRelCount
	 *            - total click count
	 */
	public void setTotalRelCount(long totalRelCount) {
		this.totalRelCount = totalRelCount;
	}

	@Override
	public String toString() {
		return "URI: " + uri + " Count: " + totalRelCount;
	}

}
