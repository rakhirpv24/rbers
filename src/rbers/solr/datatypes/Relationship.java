package rbers.solr.datatypes;

/**
 * This class represents a relationship in the dataset. A relationship can have
 * a name. This can be visualized as a resource in a doc in Solr
 *
 * @author rakhirpv24
 *
 */
public class Relationship {
	private String name;

	/**
	 * Constructor
	 *
	 * @param name
	 *            Name of the relationship
	 */
	public Relationship(String name) {
		this.name = name;
	}

	/**
	 * This gets the name of relationship
	 *
	 * @return name of Relationship
	 */
	public String getName() {
		return name;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj != null && obj instanceof Relationship) {
			Relationship rel = (Relationship) obj;
			if (this.name == null) {
				if (rel.getName() == null) {
					return true;
				} else {
					return false;
				}
			} else {
				if (rel.getName() == null) {
					return false;
				} else {
					return this.name.equals(rel.getName());
				}
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return this.name.hashCode();
	};

	@Override
	public String toString() {
		return "Relationship: " + this.name;
	}
}
