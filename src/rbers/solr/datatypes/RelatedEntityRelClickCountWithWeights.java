package rbers.solr.datatypes;

import java.util.List;

/**
 * This class stores the URI of the related entity and the total click count of
 * all those relationships of that related entity which matches user profile
 * relationships for that user
 */
public class RelatedEntityRelClickCountWithWeights {
	private String uri;
	private double weightedTotalRelCount;
	private List<Relationship> relationship;

	/*
	 * Constructor
	 */
	public RelatedEntityRelClickCountWithWeights(String uri,
			double totalRelCount, List<Relationship> relationship) {
		this.uri = uri;
		this.weightedTotalRelCount = totalRelCount;
		this.relationship = relationship;
	}

	/**
	 * Getter for URI of related entity
	 *
	 * @return uri
	 */
	public String getUri() {
		return uri;
	}

	/**
	 * Setter for URI of related entity
	 *
	 * @param uri
	 *            - URI of related entity
	 */
	public void setUri(String uri) {
		this.uri = uri;
	}

	/**
	 * Getter for total matched(with user profile of that user) relationships
	 * click count of the related entity
	 *
	 * @return total click count
	 */
	public double getWeightedTotalRelCount() {
		return weightedTotalRelCount;
	}

	/**
	 * Setter for total matched(with user profile of that user) relationships
	 * click count of the related entity
	 *
	 * @param totalRelCount
	 *            - total click count
	 */
	public void setWeightedTotalRelCount(double totalRelCount) {
		this.weightedTotalRelCount = totalRelCount;
	}

	@Override
	public String toString() {
		return "URI: " + uri + " WeightCount: " + weightedTotalRelCount;
	}

	public List<Relationship> getRelationship() {
		return relationship;
	}

	public void setRelationship(List<Relationship> relationship) {
		this.relationship = relationship;
	}

	public void addRelationshipAndWeight(Relationship rel, double relWeight) {
		this.relationship.add(rel);
		this.weightedTotalRelCount += relWeight;
	}

}
