package rbers.solr;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.lucene.index.Term;
import org.apache.lucene.search.Query;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrException;
import org.apache.solr.common.params.CommonParams;
import org.apache.solr.common.params.ModifiableSolrParams;
import org.apache.solr.common.params.ShardParams;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.handler.component.ResponseBuilder;
import org.apache.solr.handler.component.SearchComponent;
import org.apache.solr.handler.component.ShardRequest;
import org.apache.solr.handler.component.ShardResponse;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.search.QParser;
import org.apache.solr.search.QParserPlugin;
import org.apache.solr.search.QueryParsing;
import org.apache.solr.search.SyntaxError;

import rbers.solr.datatypes.Entity;
import rbers.solr.datatypes.RelatedEntityRelClickCount;
import rbers.solr.datatypes.RelatedEntityRelClickCountWithWeights;
import rbers.solr.datatypes.Relationship;
import rbers.solr.utilities.Utility;

public class RbersSearchComponent extends SearchComponent {

	// Reset all these global variables in prepare method because in one
	// session, search component object is created only once
	private Entity queriedEntity;
	private List<Relationship> matchedOrderedRelFromUserProfile,
			matchedOrderedRelFromUserContext,
			mergedRelForUser = new ArrayList<Relationship>();

	// Map of uri and the corresponding Entity which stores uri and the entity
	// objects for all related entities
	private Map<String, Entity> mapOfRelatedUriEntity = new HashMap<>();
	private int currentRelatedEntityTracker = 0;
	// Map of uri of related entity and its total relationships click count
	private Map<String, Long> mapOfRelatedEntityRelClickCount = new HashMap<>();
	// Map of relationship and the list of RelatedEntityRelClickCount
	private Map<Relationship, List<RelatedEntityRelClickCount>> mapOfRelationshipToOrderedEntitiesCount = new HashMap<>();
	private List<RelatedEntityRelClickCountWithWeights> weightedOrderedRelClickCount = new ArrayList<>();

	// variable to manage the call of handleResponses() corresponding to query
	// from distributedProcess()
	private int queryGeneratingPhase = RbersConstants.NONE_PHASE;;

	@Override
	public String getDescription() {
		return "Rbers Search Component to recommend Relationship based related Entities";
	}

	@Override
	public void prepare(ResponseBuilder rb) throws IOException {
		print("Inside prepare 1 of Rbers Search Component");
		queryGeneratingPhase = RbersConstants.NONE_PHASE;
		queriedEntity = null;
		matchedOrderedRelFromUserProfile = new ArrayList<Relationship>();
		matchedOrderedRelFromUserContext = new ArrayList<Relationship>();
		mergedRelForUser = new ArrayList<Relationship>();

		mapOfRelatedUriEntity = new HashMap<>();
		currentRelatedEntityTracker = 0;
		mapOfRelatedEntityRelClickCount = new HashMap<>();
		mapOfRelationshipToOrderedEntitiesCount = new HashMap<>();
		weightedOrderedRelClickCount = new ArrayList<>();
	}

	@Override
	public void process(ResponseBuilder rb) throws IOException {
		print("Inside process 1 of Rbers Search Component");
	}

	@Override
	public int distributedProcess(ResponseBuilder rb) throws IOException {
		if (rb.stage < ResponseBuilder.STAGE_GET_FIELDS)
			return ResponseBuilder.STAGE_GET_FIELDS;
		if (rb.stage == ResponseBuilder.STAGE_GET_FIELDS) {
			if (queryGeneratingPhase == RbersConstants.NONE_PHASE) {
				queryGeneratingPhase = RbersConstants.QUERY_ENTITY_PHASE;
			}
			SolrParams solrParams = rb.req.getParams();
			if (rb.getQueryString() == null) {
				rb.setQueryString(solrParams.get(CommonParams.Q));
			}
			print("distributedProcess() : Current Phase of Search Component:  "
					+ queryGeneratingPhase);

			switch (queryGeneratingPhase) {
			case RbersConstants.QUERY_ENTITY_PHASE:
				generateQueryForEntitySearchInData(rb);
				break;
			case RbersConstants.QUERY_ORDERED_REL_USER_PROFILE_PHASE:
				if (queriedEntity == null) {
					return ResponseBuilder.STAGE_DONE;
				}
				print("distributedProcess(): Queried ENtity has :  "
						+ queriedEntity.getRelationshipAndEntityMap().size());
				generateQueryForRelSearchForUser(rb, true);
				break;
			case RbersConstants.QUERY_ORDERED_REL_CONTEXT_PHASE:
				print("distributedProcess() : Matched Relationships are"
						+ matchedOrderedRelFromUserProfile);
				generateQueryForRelSearchForUser(rb, false);
				break;
			case RbersConstants.QUERY_RELATED_ENTITIES_PHASE:
				print("distributedProcess() : Matched Relationships in Context are"
						+ matchedOrderedRelFromUserContext);
				getMapOfRelatedUriAndEntity();
				String queryForRelatedEntities = createQueryStringForRelatedEntities();
				generateQueryForRelatedEntitiesSearchInData(rb,
						queryForRelatedEntities);
				break;
			case RbersConstants.QUERY_RELATED_ENTITIES_REL_CLICK_COUNT_PHASE:
				print("distributedProcess() : Map of Related Uri and It's Entity is:  ");
				for (String str : mapOfRelatedUriEntity.keySet()) {
					print(str
							+ "\t"
							+ mapOfRelatedUriEntity.get(str)
									.getRelationshipAndEntityMap().size());
				}
				generateQueryForRelatedEntitiesRelClickCountForUser(rb, true);
				break;
			case RbersConstants.DONE:
				print("distributedProcess() : Found total entities: "
						+ mapOfRelatedEntityRelClickCount.size());
				// for (String key : mapOfRelatedEntityRelClickCount.keySet()) {
				// print("distributedProcess(): URI: " + key + " Count: "
				// + mapOfRelatedEntityRelClickCount.get(key));
				// }
				getOrderedRelatedEntitiesPerRelationship();
				for (Relationship rel : mapOfRelationshipToOrderedEntitiesCount
						.keySet()) {
					StringBuffer sb = new StringBuffer("");
					sb.append("distributedProcess(): Relationship : "
							+ rel.getName() + " Ordered Entities:  ");
					for (RelatedEntityRelClickCount entity : mapOfRelationshipToOrderedEntitiesCount
							.get(rel)) {
						sb.append(entity.getUri() + " AND TOTAL: "
								+ entity.getTotalRelCount() + " , ");
					}
					print(sb.toString());
				}

				applyWeightsAndSort();

				print("distributedProcess(): " + weightedOrderedRelClickCount);
				generateResponse(rb);
				return ResponseBuilder.STAGE_DONE;

			default:
				break;
			}
			return ResponseBuilder.STAGE_GET_FIELDS;
		}
		return ResponseBuilder.STAGE_DONE;

	}

	@Override
	public void init(NamedList args) {
		super.init(args);
		print("Inside init 1 of Rbers Search Component");

	}

	@Override
	public void handleResponses(ResponseBuilder rb, ShardRequest sreq) {
		print("In handleResponse method ..Current Phase of Search Component:  "
				+ queryGeneratingPhase);

		switch (queryGeneratingPhase) {
		case RbersConstants.QUERY_ENTITY_PHASE:
			retrieveResponseForEntitySearchInData(rb, sreq);
			break;
		case RbersConstants.QUERY_ORDERED_REL_USER_PROFILE_PHASE:
			retrieveResponseForRelSearchForUser(rb, sreq, true);
			break;
		case RbersConstants.QUERY_ORDERED_REL_CONTEXT_PHASE:
			retrieveResponseForRelSearchForUser(rb, sreq, false);
			mergeRelFromProfileAndContext();
			break;
		case RbersConstants.QUERY_RELATED_ENTITIES_PHASE:
			retrieveResponseForRelatedEntitiesSearchInData(rb, sreq);
			break;
		case RbersConstants.QUERY_RELATED_ENTITIES_REL_CLICK_COUNT_PHASE:
			retrieveResponseForRelatedEntitiesRelClickCountForUser(rb, sreq,
					true);
			break;
		case RbersConstants.DONE:
		default:
			break;
		}
	}

	private void print(String str) {
		System.out.println("**************" + str + "\n");
	}

	private Query parseQuery(String query, SolrParams solrParams,
			SolrQueryRequest req) {
		Query enteredQuery = null;
		String defType = solrParams.get(QueryParsing.DEFTYPE,
				QParserPlugin.DEFAULT_QTYPE);

		try {
			QParser parser = QParser.getParser(query, defType, req);
			enteredQuery = parser.getQuery();
			print(parser.getQuery().toString());
		} catch (SyntaxError e) {
			throw new SolrException(SolrException.ErrorCode.BAD_REQUEST, e);
		}
		return enteredQuery;
	}

	private Map<String, String> getQueryTerms(String query,
			SolrParams solrParams, SolrQueryRequest req) {
		Query parsedQuery = parseQuery(query, solrParams, req);
		Set<Term> setOfTerms = new HashSet<Term>();
		parsedQuery.extractTerms(setOfTerms);

		Map<String, String> queryFieldAndValue = new HashMap<>();
		Iterator<Term> iteratorOfTerms = setOfTerms.iterator();
		while (iteratorOfTerms.hasNext() == true) {
			Term term = iteratorOfTerms.next();
			queryFieldAndValue.put(term.field(), term.text());
		}
		return queryFieldAndValue;
	}

	/**
	 * This method generates the query to search the queriedEntity in dataset
	 * and add this query to ResponseBuilder
	 *
	 * @param rb
	 *            - Object of ResponseBuilder
	 */
	private void generateQueryForEntitySearchInData(ResponseBuilder rb) {
		ShardRequest sreq = new ShardRequest();
		sreq.params = new ModifiableSolrParams(rb.req.getParams());
		sreq.params.remove(ShardParams.SHARDS);
		Map<String, String> mapOfTerms = getQueryTerms(rb.getQueryString(),
				rb.req.getParams(), rb.req);
		StringBuffer sb = new StringBuffer("");
		sb.append(RbersConstants.URI).append(":\"")
				.append(mapOfTerms.get(RbersConstants.URI)).append("\"");
		sreq.params.set(CommonParams.Q, sb.toString());
		rb.shards = new String[] { RbersConstants.SHARD_DATA };
		print(sreq.toString());
		rb.addRequest(this, sreq);

	}

	/**
	 * This method retrieves response when the queried entity search query is
	 * run on dataset and after successful retrieval, this method sets the
	 * queryGeneratingphase to value of 2
	 *
	 * @param rb
	 *            - Object of ResponseBuilder
	 * @param sreq
	 *            - Object of ShardRequest
	 */
	private void retrieveResponseForEntitySearchInData(ResponseBuilder rb,
			ShardRequest sreq) {
		for (ShardResponse srsp : sreq.responses) {
			SolrDocumentList docs = (SolrDocumentList) srsp.getSolrResponse()
					.getResponse().get("response");
			if (docs.size() == 0) {
				print("retrieveResponseForEntitySearchInData(): Data for given entity not found");
				queryGeneratingPhase = RbersConstants.DONE;
				return;
			}
			try {
				queriedEntity = Utility.getEntity(docs);
				queryGeneratingPhase = RbersConstants.QUERY_ORDERED_REL_USER_PROFILE_PHASE;
			} catch (Exception e) {
				print("Exception occured in getting object of Entity");
				e.printStackTrace();
				queryGeneratingPhase = RbersConstants.DONE;
			}

		}

	}

	/**
	 * This method generates a query to be send to user profile or user context
	 * core in order to get the ordered list of matching relationships based on
	 * click count. This query would be in the form of user_id:<user id> AND
	 * (relationship:<rel1> OR relationship:<rel2>...)
	 *
	 * @param rb
	 *            - Object of ResponseBuilder
	 * @param isUserProfile
	 *            - boolean variable to distinguish between user profile and
	 *            user context core to be used
	 */
	private void generateQueryForRelSearchForUser(ResponseBuilder rb,
			boolean isUserProfile) {
		ShardRequest sreq = new ShardRequest();
		sreq.params = new ModifiableSolrParams(rb.req.getParams());
		sreq.params.remove(ShardParams.SHARDS);

		Map<String, String> mapOfTerms = getQueryTerms(rb.getQueryString(),
				rb.req.getParams(), rb.req);
		StringBuffer sb = new StringBuffer("");
		sb.append(RbersConstants.USER_ID).append(":\"")
				.append(mapOfTerms.get(RbersConstants.USER_ID)).append("\"");
		String relOredQuery = Utility.createQueryFromEntity(queriedEntity);
		sb.append(" AND (").append(relOredQuery).append(")");

		sreq.params.set(CommonParams.Q, sb.toString());
		sreq.params
				.set(CommonParams.SORT, RbersConstants.CLICK_COUNT + " desc");
		if (isUserProfile == true) {
			rb.shards = new String[] { RbersConstants.SHARD_USER_PROFILE };
		} else {
			rb.shards = new String[] { RbersConstants.SHARD_USER_CONTEXT };
		}
		print(sreq.toString());
		rb.addRequest(this, sreq);

	}

	/**
	 * This method is to retrieve response of ordered matching relationships of
	 * queried entity from user profile or user context core, based on click
	 * count. On successful retrieval of response, this method sets the value of
	 * queryGeneratingPhase variable to 3(if the query is run on userProfile) or
	 * to 4(if the query is run on user context core)
	 *
	 * @param rb
	 *            - Object of ResponseBuilder
	 * @param sreq
	 *            - Object of ShardRequest
	 * @param isUserProfile
	 *            - boolean variable to distinguish between user profile and
	 *            user context core to be used
	 */
	private void retrieveResponseForRelSearchForUser(ResponseBuilder rb,
			ShardRequest sreq, boolean isUserProfile) {
		for (ShardResponse srsp : sreq.responses) {
			SolrDocumentList docs = (SolrDocumentList) srsp.getSolrResponse()
					.getResponse().get("response");
			if (docs.size() == 0) {
				print("retrieveResponseForRelSearchForUser(): Data for given user not found");
				queryGeneratingPhase = RbersConstants.DONE;
				return;
			}
			try {
				if (isUserProfile == true) {
					matchedOrderedRelFromUserProfile = Utility
							.getOrderedRel(docs);

					queryGeneratingPhase = RbersConstants.QUERY_ORDERED_REL_CONTEXT_PHASE;
				} else {
					matchedOrderedRelFromUserContext = Utility
							.getOrderedRel(docs);

					queryGeneratingPhase = RbersConstants.QUERY_RELATED_ENTITIES_PHASE;
				}
			} catch (Exception e) {
				print("Exception occured in getting object of Entity");
				e.printStackTrace();
				queryGeneratingPhase = RbersConstants.DONE;
			}

		}
	}

	/**
	 * This method merges the ordered matching relationships retrieved from
	 * userProfile and userContext core. Relationships would be merged in the
	 * list: mergedRelForUser, in the order: relationships from user context
	 * core and then relationships from user profile core and then the remaining
	 * relationships which are associated with queriedEntity but are not present
	 * in both user profile and user context core.
	 */
	private void mergeRelFromProfileAndContext() {

		mergedRelForUser.addAll(matchedOrderedRelFromUserContext);

		for (Relationship rel : matchedOrderedRelFromUserProfile) {
			if (mergedRelForUser.contains(rel) == false) {
				mergedRelForUser.add(rel);
			}
		}
		// Merge the relationships which are not present in User profile and
		// context at the end of the list.
		for (Relationship rel : queriedEntity.getRelationshipAndEntityMap()
				.keySet()) {
			if (mergedRelForUser.contains(rel) == false) {
				mergedRelForUser.add(rel);
			}
		}
	}

	/**
	 * Method to get details of all related entities for the given entity. For
	 * every relationship in mergedRelForUser, it will add an entry of uri and
	 * the corresponding related entity
	 */
	private void getMapOfRelatedUriAndEntity() {
		Map<Relationship, List<Entity>> mapOfDetailsOfQueriedEntity = queriedEntity
				.getRelationshipAndEntityMap();
		for (Relationship rel : mergedRelForUser) {
			if (mapOfDetailsOfQueriedEntity.containsKey(rel)) {
				List<Entity> entities = mapOfDetailsOfQueriedEntity.get(rel);
				for (Entity e : entities) {
					mapOfRelatedUriEntity.put(e.getUniqueIdentifier(), e);
				}
			}
		}
	}

	/**
	 * This method creates a query string to search related entities in dataset
	 *
	 * @return query string
	 */
	private String createQueryStringForRelatedEntities() {
		StringBuffer sb = new StringBuffer("");
		Set<String> set = mapOfRelatedUriEntity.keySet();
		int i = 0;
		for (String s : set) {
			if (i < set.size() - 1) {
				sb.append(RbersConstants.URI).append(":").append("\"")
						.append(s).append("\"").append(" OR ");
			} else {
				sb.append(RbersConstants.URI).append(":").append("\"")
						.append(s).append("\"");
			}
			i++;
		}
		return sb.toString();
	}

	/**
	 * This method adds the query string created for related entities search in
	 * dataset, to ResponseBuilder
	 *
	 * @param rb
	 *            - Object of ResponseBuilder
	 * @param queryString
	 *            - query string created to search related entities details in
	 *            dataset
	 */
	private void generateQueryForRelatedEntitiesSearchInData(
			ResponseBuilder rb, String queryString) {
		ShardRequest sreq = new ShardRequest();
		sreq.params = new ModifiableSolrParams(rb.req.getParams());
		sreq.params.remove(ShardParams.SHARDS);
		sreq.params.set(CommonParams.Q, queryString);
		rb.shards = new String[] { RbersConstants.SHARD_DATA };
		print(sreq.toString());
		rb.addRequest(this, sreq);
	}

	/**
	 * This method retrieves the response of query to search the details of
	 * related entities in dataset. After successful retrieval of related
	 * entities information, this method sets the queryGeneratingPhase to 5
	 *
	 * @param rb
	 *            - Object of ResponseBuilder
	 * @param sreq
	 *            - Object of ShardRequest
	 */
	private void retrieveResponseForRelatedEntitiesSearchInData(
			ResponseBuilder rb, ShardRequest sreq) {
		for (ShardResponse srsp : sreq.responses) {
			SolrDocumentList docs = (SolrDocumentList) srsp.getSolrResponse()
					.getResponse().get("response");
			if (docs.size() == 0) {
				print("retrieveResponseForRelatedEntitiesSearchInData(): Data for given entities not found");
				// TODO: think of handling the case when related docs don't have
				// data
				queryGeneratingPhase = RbersConstants.DONE;
				return;
			}
			try {
				Utility.updateEntityInMapForEachDoc(docs, mapOfRelatedUriEntity);
				queryGeneratingPhase = RbersConstants.QUERY_RELATED_ENTITIES_REL_CLICK_COUNT_PHASE;
			} catch (Exception e) {
				print("retrieveResponseForRelatedEntitiesSearchInData() : Exception occured in getting object of Entity");
				e.printStackTrace();
				queryGeneratingPhase = RbersConstants.DONE;
			}

		}

	}

	private void generateQueryForRelatedEntitiesRelClickCountForUser(
			ResponseBuilder rb, boolean isUserProfile) {
		ShardRequest sreq = new ShardRequest();
		sreq.params = new ModifiableSolrParams(rb.req.getParams());
		sreq.params.remove(ShardParams.SHARDS);

		Map<String, String> mapOfTerms = getQueryTerms(rb.getQueryString(),
				rb.req.getParams(), rb.req);
		StringBuffer sb = new StringBuffer("");
		sb.append(RbersConstants.USER_ID).append(":\"")
				.append(mapOfTerms.get(RbersConstants.USER_ID)).append("\"");

		String uri = null;
		int i = 0;
		Iterator<String> it = mapOfRelatedUriEntity.keySet().iterator();
		Entity queriedEntity = null;
		while (it.hasNext() && i <= currentRelatedEntityTracker) {
			uri = it.next();
			if (i == currentRelatedEntityTracker
					&& i < mapOfRelatedUriEntity.size()) {
				queriedEntity = mapOfRelatedUriEntity.get(uri);
				print("generateQueryForRelatedEntitiesRelClickCountForUser(): Querying for uri: "
						+ uri);
				// if (mapOfRelatedEntityRelClickCount.get(uri) == null) {
				if (queriedEntity.getRelationshipAndEntityMap().size() == 0) {
					print("generateQueryForRelatedEntitiesRelClickCountForUser(): No related entities data found, so putting count as 0.");
					mapOfRelatedEntityRelClickCount.put(uri, 0L);
					currentRelatedEntityTracker++;
				}
				// print("generateQueryForRelatedEntitiesRelClickCountForUser(): "
				// + uri + " not found on map");
				// } else {
				// print("generateQueryForRelatedEntitiesRelClickCountForUser(): "
				// + uri
				// + " found on map with count: "
				// + mapOfRelatedEntityRelClickCount.get(uri));
				// currentRelatedEntityTracker++;
				// }
			}
			i++;
		}
		/*
		 * All the items in the iterator are exhausted and i is equal to or
		 * greater than mapOfRelatedUriEntity.size(). For the case when i
		 * represents the last entity to be searched, if its related entity map
		 * is null, then mark the phase as done, otherwise run the query to get
		 * data of this last related entity.
		 */
		if (i > mapOfRelatedUriEntity.size()
				|| (queriedEntity == null || queriedEntity
						.getRelationshipAndEntityMap().size() == 0)) {
			queryGeneratingPhase = RbersConstants.DONE;
			return;
		}

		// // TODO: Optimize to save a round of running
		// queriedEntity = mapOfRelatedUriEntity.get(uri);
		// if (queriedEntity.getRelationshipAndEntityMap().size() == 0) {
		// currentRelatedEntityTracker++;
		// return;
		// }

		String relOredQuery = Utility.createQueryFromEntity(queriedEntity);
		sb.append(" AND (").append(relOredQuery).append(")");

		sreq.params.set(CommonParams.Q, sb.toString());

		if (isUserProfile == true) {
			rb.shards = new String[] { RbersConstants.SHARD_USER_PROFILE };
		} else {
			rb.shards = new String[] { RbersConstants.SHARD_USER_CONTEXT };
		}
		print(sreq.toString());
		rb.addRequest(this, sreq);

	}

	private void retrieveResponseForRelatedEntitiesRelClickCountForUser(
			ResponseBuilder rb, ShardRequest sreq, boolean isUserProfile) {
		for (ShardResponse srsp : sreq.responses) {
			SolrDocumentList docs = (SolrDocumentList) srsp.getSolrResponse()
					.getResponse().get("response");
			if (docs.size() == 0) {
				print("retrieveResponseForRelSearchInUserProfile(): Data for given user not found");
				// queryGeneratingPhase = RbersConstants.DONE;
				mapOfRelatedEntityRelClickCount.put(
						queriedEntity.getUniqueIdentifier(), 0L);
				currentRelatedEntityTracker++;
				return;
			}
			try {
				if (isUserProfile == true) {
					print("retrieveResponseForRelSearchInUserProfile(): Found docs: "
							+ docs.size());
					String uri = null;
					int i = 0;
					Iterator<String> it = mapOfRelatedUriEntity.keySet()
							.iterator();
					while (it.hasNext() && i <= currentRelatedEntityTracker) {
						uri = it.next();
						i++;
					}
					// if (i > mapOfRelatedUriEntity.size()) {
					// queryGeneratingPhase = RbersConstants.DONE;
					// return;
					// }
					Entity queriedEntity = mapOfRelatedUriEntity.get(uri);

					RelatedEntityRelClickCount r = Utility
							.calculateRelatedEntityRelClickCountTotal(docs,
									queriedEntity);
					if (r != null) {
						mapOfRelatedEntityRelClickCount.put(
								queriedEntity.getUniqueIdentifier(),
								r.getTotalRelCount());
						print("retrieveResponseForRelSearchInUserProfile(): Uri: "
								+ queriedEntity.getUniqueIdentifier()
								+ " and click count: " + r.getTotalRelCount());
					} else {
						mapOfRelatedEntityRelClickCount.put(
								queriedEntity.getUniqueIdentifier(), 0L);
					}
					currentRelatedEntityTracker++;
					if (i >= mapOfRelatedUriEntity.size()) {
						queryGeneratingPhase = RbersConstants.DONE;
						return;
					}
				}
				// } else {
				// matchedOrderedRelFromUserContext = Utility
				// .getOrderedRel(docs);
				//
				// queryGeneratingPhase =
				// RbersConstants.QUERY_RELATED_ENTITIES_PHASE;
				// }
			} catch (Exception e) {
				print("Exception occured in getting object of Entity");
				e.printStackTrace();
				queryGeneratingPhase = RbersConstants.DONE;
			}
		}
	}

	private void getOrderedRelatedEntitiesPerRelationship() {
		Comparator<Entity> c = new Comparator<Entity>() {
			@Override
			public int compare(Entity e1, Entity e2) {
				Long e1Count = mapOfRelatedEntityRelClickCount.get(e1
						.getUniqueIdentifier());
				Long e2Count = mapOfRelatedEntityRelClickCount.get(e2
						.getUniqueIdentifier());
				if (e1Count == null && e2Count == null) {
					return 0;
				}
				if (e1Count == null) {
					return 1;
				}
				if (e2Count == null) {
					return -1;
				}
				return (int) (e2Count - e1Count);
			}
		};
		for (Relationship rel : mergedRelForUser) {
			List<Entity> entities = queriedEntity.getRelationshipAndEntityMap()
					.get(rel);
			Collections.sort(entities, c);

			List<RelatedEntityRelClickCount> orderedEntitiesPerRel = new ArrayList<>();
			for (Entity entity : entities) {
				String uri = entity.getUniqueIdentifier();
				Long relClickCount = mapOfRelatedEntityRelClickCount.get(uri);
				orderedEntitiesPerRel.add(new RelatedEntityRelClickCount(uri,
						relClickCount == null ? 0L : relClickCount));
			}
			mapOfRelationshipToOrderedEntitiesCount.put(rel,
					orderedEntitiesPerRel);
		}
	}

	private void applyWeightsAndSort() {
		double relWeight = 1;
		double entityWeight = 1;

		print("applyWeightsAndSort(): mapOfRelationshipToOrderedEntitiesCount : \n"
				+ mapOfRelationshipToOrderedEntitiesCount);
		Map<String, RelatedEntityRelClickCountWithWeights> uriToWeightMap = new HashMap<>();
		for (Relationship rel : mergedRelForUser) {
			entityWeight = 1;
			List<RelatedEntityRelClickCount> tempList = mapOfRelationshipToOrderedEntitiesCount
					.get(rel);

			for (RelatedEntityRelClickCount r : tempList) {
				RelatedEntityRelClickCountWithWeights weightObj = uriToWeightMap
						.get(r.getUri());
				if (weightObj != null) {
					weightObj.addRelationshipAndWeight(rel,
							r.getTotalRelCount() * (1 / relWeight)
									* (1 / entityWeight));
				} else {
					List<Relationship> relList = new ArrayList<>();
					relList.add(rel);
					weightObj = new RelatedEntityRelClickCountWithWeights(
							r.getUri(), r.getTotalRelCount() * (1 / relWeight)
									* (1 / entityWeight), relList);
					weightedOrderedRelClickCount.add(weightObj);
					uriToWeightMap.put(r.getUri(), weightObj);
				}
				entityWeight++;
			}
			relWeight++;
		}
		Collections.sort(weightedOrderedRelClickCount,
				new Comparator<RelatedEntityRelClickCountWithWeights>() {
					@Override
					public int compare(
							RelatedEntityRelClickCountWithWeights r1,
							RelatedEntityRelClickCountWithWeights r2) {
						double r1Weight = r1.getWeightedTotalRelCount();
						double r2Weight = r2.getWeightedTotalRelCount();
						if (r2Weight == r1Weight) {
							return 0;
						}
						if (r2Weight > r1Weight) {
							return 1;
						}
						return -1;
					}
				});
	}

	private void generateResponse(ResponseBuilder rb) {
		SolrDocumentList docs = new SolrDocumentList();
		if (weightedOrderedRelClickCount.isEmpty() == false) {
			docs.setNumFound(weightedOrderedRelClickCount.size());
			for (RelatedEntityRelClickCountWithWeights r : weightedOrderedRelClickCount) {
				SolrDocument doc = new SolrDocument();
				doc.addField(RbersConstants.URI, r.getUri());
				for (Relationship rel : r.getRelationship()) {
					doc.addField(RbersConstants.RELATIONSHIP, rel.getName());
				}
				docs.add(doc);
			}
		} else {
			if (queriedEntity != null) {
				int totalEntities = 0;
				Map<String, SolrDocument> entityToDocs = new HashMap<>();
				for (Relationship rel : queriedEntity
						.getRelationshipAndEntityMap().keySet()) {
					for (Entity entity : queriedEntity
							.getRelationshipAndEntityMap().get(rel)) {
						SolrDocument doc = entityToDocs.get(entity
								.getUniqueIdentifier());
						if (doc != null) {
							doc.addField(RbersConstants.RELATIONSHIP,
									rel.getName());
						} else {
							doc = new SolrDocument();
							doc.addField(RbersConstants.URI,
									entity.getUniqueIdentifier());
							doc.addField(RbersConstants.RELATIONSHIP,
									rel.getName());
							docs.add(doc);
							totalEntities++;
							entityToDocs.put(entity.getUniqueIdentifier(), doc);
						}
					}
					docs.setNumFound(totalEntities);
				}
			}
		}
		rb.rsp.add("response", docs);
	}
}
