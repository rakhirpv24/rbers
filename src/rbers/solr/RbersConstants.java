package rbers.solr;

public class RbersConstants {
	public static final String URI = "uri";
	public static final String USER_ID = "user_id";
	public static final String CLICK_COUNT = "click_count";
	public static final String RELATIONSHIP = "relationship";

	public static final String SHARD_DATA = "localhost:8983/solr/cs298-collection";
	public static final String SHARD_USER_PROFILE = "localhost:8983/solr/cs298-userprofile";
	public static final String SHARD_USER_CONTEXT = "localhost:8983/solr/cs298-usercontext";

	public static final int QUERY_ENTITY_PHASE = 1;
	public static final int QUERY_ORDERED_REL_USER_PROFILE_PHASE = 2;
	public static final int QUERY_ORDERED_REL_CONTEXT_PHASE = 3;
	public static final int QUERY_RELATED_ENTITIES_PHASE = 4;
	public static final int QUERY_RELATED_ENTITIES_REL_CLICK_COUNT_PHASE = 5;
	public static final int DONE = 6;
	public static final int NONE_PHASE = -1;
	public static final String QUERY_GENERATING_PHASE = "queryGeneratingPhase";
}
