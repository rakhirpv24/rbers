package rbers.common;

/**
 * This class contains all the constants and their values which are used in
 * various classes in this project
 *
 * @author rakhirpv24
 *
 */
public class Constants {

	public static final String SOLR_HOME = "C:\\solr-5.0.0\\solr-5.0.0\\cs298\\solr";
	public static final String SOLR_SERVER_HOME = "C:\\solr-5.0.0\\solr-5.0.0\\server\\solr";
	public static final String SOLR_CS_298_COLLECTION_URL = "http://localhost:8983/solr/cs298-collection";
	public static final String SOLR_CS_298_PROJECT_URL = "http://localhost:8983/solr/cs298-project";
	public static final String SOLR_CS_298_USER_PROFILE_URL = "http://localhost:8983/solr/cs298-userprofile";
	public static final String SOLR_CS_298_USER_CONTEXT_URL = "http://localhost:8983/solr/cs298-usercontext";
	public static final String DBPEDIA_URI_PREFIX = "http://dbpedia.org/resource/";
	public static final String SOLR_URI = "uri";
	public static final String SOLR_USER_ID = "user_id";
	public static final String SOLR_RELATIONSHIP = "relationship";
	public static final String SOLR_CLICK_COUNT = "click_count";
	public static final String SOLR_USER_PROFILE_ID = "uri";
	public static final int NUMBER_OF_RELATIONSHIPS_TO_SHOW = 7;
}
