package rbers;

import java.util.ArrayList;
import java.util.List;

import rbers.common.Constants;
import rbers.dataaccess.DataRetrieverFromKB;
import rbers.datatypes.Entity;
import rbers.datatypes.Relationship;

/**
 * This class recommends the entities to the user based on the entity searched
 * i.e., input entity and the user profile.
 *
 * @author rakhirpv24
 *
 */
public class EntityRecommender {

	/**
	 * This method calculates the relationships user is interested in, based on
	 * the click count of relationships in user profile. Higher the click count,
	 * higher the rank of the relationship and will be shown first
	 *
	 * @param dataRetriever
	 *            Object of DataRetrieverFromKB
	 * @param queriedEntity
	 *            Object of Entity
	 * @param userName
	 *            Name of the user whose user profile has to be taken into
	 *            consideration for entities recommendation
	 * @return Ordered List of Relationships user is interested in, based on
	 *         click count in user profile
	 * @throws Exception
	 */
	// TODO: if searching in user profile is over and my result set has not been
	// reached to the upper limit then i will have to add the relationships from
	// the dataset related to searched entity
	public List<Relationship> getOrderedRelationshipsForEntity(
			DataRetrieverFromKB dataRetriever, Entity queriedEntity,
			String userName) throws Exception {
		int currentOffset = 0;
		int numberOfRelationshipsPerQuery = 100;

		List<Relationship> result = new ArrayList<>();
		while (result.size() < Constants.NUMBER_OF_RELATIONSHIPS_TO_SHOW) {
			try {
				result.addAll(dataRetriever.getOrderedRelationshipsForUser(
						userName, queriedEntity, currentOffset,
						numberOfRelationshipsPerQuery));
				currentOffset += numberOfRelationshipsPerQuery;
			} catch (Exception e) {
				if (e.getMessage().startsWith("Unable to find")) {
					return result;
				}
			}
		}
		return result.size() <= Constants.NUMBER_OF_RELATIONSHIPS_TO_SHOW ? result
				: result.subList(0, Constants.NUMBER_OF_RELATIONSHIPS_TO_SHOW);
	}
}
