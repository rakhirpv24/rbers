package rbers.dataaccess;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrDocumentList;

/**
 * This is a utility class which takes query string and the solr connection as
 * input and set that query in SolrQuery and execute it. It returns object of
 * SolrDocumentList which basically contain a list of all the resultant
 * documents from the query
 *
 * @author rakhirpv24
 *
 */
public class SolrJQueryUtility {

	/**
	 * This gets all the resultant documents for the given query
	 *
	 * @param solrConnection
	 *            connection URL of Solr where the dataset is kept
	 * @param queryString
	 *            Query String
	 * @return All the resultant docs matching the given query
	 * @throws SolrServerException
	 */
	public static SolrDocumentList query(HttpSolrServer solrConnection,
			String queryString) throws SolrServerException {
		SolrQuery solrQuery = new SolrQuery();
		solrQuery.setQuery(queryString);
		return solrConnection.query(solrQuery).getResults();
	}
}
