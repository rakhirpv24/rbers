package rbers.dataaccess;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrQuery.ORDER;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;
import org.apache.solr.common.SolrInputDocument;

import rbers.common.Constants;
import rbers.datatypes.Entity;
import rbers.datatypes.RecommendedEntity;
import rbers.datatypes.Relationship;

/**
 * This represents the Data Retriever from Solr Knowledge Base. It implements
 * the interface DataRetrieverFromKB, which is irrespective of the underlying
 * knowledge base container. It has two connections - One is with dataset stored
 * in Solr and the other connection is with the user profile stored in Solr
 *
 * @author rakhirpv24
 *
 */
public class SolrJDataRetrieverFromKB implements DataRetrieverFromKB {

	private static HttpSolrServer solrDataConnection = new HttpSolrServer(
			Constants.SOLR_CS_298_COLLECTION_URL);

	private static HttpSolrServer solrUserProfileConnection = new HttpSolrServer(
			Constants.SOLR_CS_298_USER_PROFILE_URL);

	private static HttpSolrServer solrUserContextConnection = new HttpSolrServer(
			Constants.SOLR_CS_298_USER_CONTEXT_URL);

	private static HttpSolrServer solrProjectConnection = new HttpSolrServer(
			Constants.SOLR_CS_298_PROJECT_URL);

	@Override
	public Entity getNewEntity(String entityName) throws Exception {
		String entityUri = getEntityUri(entityName);
		try {
			SolrDocumentList results = SolrJQueryUtility.query(
					solrDataConnection, Constants.SOLR_URI + ":" + "\""
							+ entityUri + "\"");
			Entity queriedEntity = new Entity(entityUri);

			// URI is unique for all entities
			if (results != null && results.size() == 1) {
				SolrDocument solrDoc = results.get(0);
				Collection<String> fieldNames = solrDoc.getFieldNames();
				for (String field : fieldNames) {
					if (field.contains("_resource") == false) {
						continue;
					}
					List<String> allValuesOfField = null;
					Object fieldValue = solrDoc.getFieldValue(field);
					if (fieldValue instanceof String) {
						allValuesOfField = Arrays.asList((String) fieldValue);
					} else if (fieldValue instanceof ArrayList) {
						allValuesOfField = (ArrayList<String>) fieldValue;
					}
					Relationship relationship = new Relationship(field);
					List<Entity> relatedEntities = new ArrayList<>();
					for (String fieldValueString : allValuesOfField) {
						Entity relatedEntity = new Entity(fieldValueString);
						relatedEntities.add(relatedEntity);
					}
					queriedEntity.addRelationshipAndEntity(relationship,
							relatedEntities);
				}
				return queriedEntity;
			} else {
				if (results == null || results.size() == 0) {
					throw new Exception("Found 0 Documents for given URI: "
							+ entityUri);
				} else {
					throw new Exception("Found " + results.size()
							+ " Documents for given URI: " + entityUri);
				}
			}
		} catch (SolrServerException e) {
			throw new Exception("Connection to solr server at URL: "
					+ Constants.SOLR_CS_298_COLLECTION_URL + " has issues.", e);
		}
	}

	@Override
	public void closeConnection() {
		if (solrDataConnection != null) {
			solrDataConnection.shutdown();
		}
	}

	/**
	 * This gets the entity name given by the user and appends the DBpedia
	 * prefix to it and returns the resultant unique identifier for the given
	 * entity
	 *
	 * @param entityName
	 *            Name of the input entity user is looking for
	 * @return unique identifier of the input entity
	 */
	private static String getEntityUri(String entityName) {
		if (entityName.startsWith(Constants.DBPEDIA_URI_PREFIX)) {
			return entityName;
		}
		return Constants.DBPEDIA_URI_PREFIX + entityName.replace(' ', '_');
	}

	/*
	 * The method can be updated to limit the number of documents returned by
	 * the SOLR query. User query.setRows(numberOfRecordsPerQuery);
	 */
	@Override
	public List<Relationship> getOrderedRelationshipsForUser(String userName,
			Entity queriedEntity, int startingOffset,
			int numberOfRelationshipsThisQuery) throws Exception {
		List<Relationship> orderedRelationshipsFromUserProfile = getRelationshipForUser(
				solrUserProfileConnection, userName, queriedEntity,
				startingOffset, numberOfRelationshipsThisQuery);

		List<Relationship> orderedRelationshipsFromUserContext = getRelationshipForUser(
				solrUserContextConnection, userName, queriedEntity,
				startingOffset, numberOfRelationshipsThisQuery);

		List<Relationship> mergedOrderedRelationships = new ArrayList<>();

		mergedOrderedRelationships.addAll(orderedRelationshipsFromUserContext);

		for (Relationship rel : orderedRelationshipsFromUserProfile) {
			if (mergedOrderedRelationships.contains(rel) == false) {
				mergedOrderedRelationships.add(rel);
			}
		}
		return mergedOrderedRelationships;
	}

	private List<Relationship> getRelationshipForUser(
			HttpSolrServer solrConnection, String userName,
			Entity queriedEntity, int startingOffset,
			int numberOfRelationshipsThisQuery) throws Exception {
		List<Relationship> result = new ArrayList<>();
		try {
			String queryWithRelationshipNames = SolrJDataRetrieverFromKB
					.createQueryFromEntity(queriedEntity);
			SolrQuery query = new SolrQuery();

			/*
			 * Query will be of the form:
			 *
			 * username:<userName> AND (relationship:<relationshipName> OR
			 * ...relationship:<relationshipName>)
			 */
			query.setQuery(Constants.SOLR_USER_ID + ":" + userName + " AND ("
					+ queryWithRelationshipNames + ")");
			query.addSort(Constants.SOLR_CLICK_COUNT, ORDER.desc);
			query.setStart(startingOffset);
			query.setRows(numberOfRelationshipsThisQuery);

			// Result of query contains all docs for a given user.
			SolrDocumentList results = solrConnection.query(query).getResults();
			List<Relationship> relationships = new ArrayList<>();
			// URI is unique for all entities
			if (results != null && results.size() != 0) {
				for (SolrDocument solrDoc : results) {
					Collection<String> fieldNames = solrDoc.getFieldNames();
					String relationshipName = null;
					for (String fieldName : fieldNames) {
						if (fieldName.contains(Constants.SOLR_RELATIONSHIP)) {
							relationshipName = (String) solrDoc
									.getFieldValue(fieldName);
						}
					}
					relationships.add(new Relationship(relationshipName));
				}
				return relationships;
			} else {
				throw new Exception("Unable to find data for user: " + userName);
			}
		} catch (SolrServerException e) {
			throw new Exception("Connection to solr server at URL: "
					+ Constants.SOLR_CS_298_USER_PROFILE_URL + " has issues.",
					e);
		}
	}

	@Override
	public void increaseUserRelationshipClickCount(String userName,
			List<Relationship> relationships) throws Exception {
		try {
			List<HttpSolrServer> solrConnections = Arrays.asList(
					solrUserContextConnection, solrUserProfileConnection);

			// Username and RelationshipName combination is unique for all
			// for a user profile
			for (Relationship relationship : relationships) {
				SolrQuery query = new SolrQuery();
				query.setQuery(Constants.SOLR_USER_ID + ":" + userName
						+ " AND " + Constants.SOLR_RELATIONSHIP + ":"
						+ relationship.getName());
				query.set("defType", "edismax");
				for (HttpSolrServer solrConnection : solrConnections) {
					// Result of query contains all docs for a given user.
					SolrDocumentList results = solrConnection.query(query)
							.getResults();

					if (results != null && results.size() == 1) {
						SolrDocument solrDoc = results.get(0);
						SolrInputDocument updateDoc = new SolrInputDocument();
						updateDoc
								.addField(
										Constants.SOLR_USER_PROFILE_ID,
										solrDoc.getFieldValue(Constants.SOLR_USER_PROFILE_ID));
						Map<String, String> incrementCountMap = new HashMap<>();
						incrementCountMap.put("inc", "1");
						updateDoc.addField(Constants.SOLR_CLICK_COUNT,
								incrementCountMap);
						solrConnection.add(updateDoc);
						solrConnection.commit();
					} else {
						if (results == null || results.size() == 0) {
							SolrInputDocument newDoc = new SolrInputDocument();
							newDoc.addField(Constants.SOLR_USER_PROFILE_ID,
									userName + "_" + relationship.getName());
							newDoc.addField(Constants.SOLR_USER_ID, userName);
							newDoc.addField(Constants.SOLR_RELATIONSHIP,
									relationship.getName());
							newDoc.addField(Constants.SOLR_CLICK_COUNT, 1);
							solrConnection.add(newDoc);
							solrConnection.commit();
						} else {
							throw new Exception(
									"Found "
											+ results.size()
											+ " combinations of username and relationship for user: "
											+ userName + " and relationship"
											+ relationship.getName()
											+ " instead of 1 combination.");
						}
					}
				}
			}
		} catch (SolrServerException e) {
			throw new Exception("Connection to solr server at URL: "
					+ Constants.SOLR_CS_298_USER_PROFILE_URL + " has issues.",
					e);
		}
	}

	private static String createQueryFromEntity(Entity queriedEntity) {
		StringBuffer relationshipStringForQuery = new StringBuffer("");
		int i = 0, totalRelationshipsInQueriedEntity = queriedEntity
				.getRelationshipAndEntityMap().keySet().size();
		for (Relationship entityRel : queriedEntity
				.getRelationshipAndEntityMap().keySet()) {
			relationshipStringForQuery.append(Constants.SOLR_RELATIONSHIP)
					.append(":").append(entityRel.getName());

			// Do not append "OR" after the last relationship
			if (i < totalRelationshipsInQueriedEntity - 1) {
				relationshipStringForQuery.append(" OR ");
			}
			i++;
		}
		return relationshipStringForQuery.toString();
	}

	@Override
	public void clearUserContext(String userName) throws Exception {
		solrUserContextConnection.deleteByQuery(Constants.SOLR_USER_ID + ":"
				+ userName);
		solrUserContextConnection.commit();
	}

	@Override
	public List<RecommendedEntity> getRecommendedEntities(String userName,
			String queriedEntityUri) throws Exception {
		try {
			SolrDocumentList results = SolrJQueryUtility.query(
					solrProjectConnection,
					generateQueryForProjectCollection(userName,
							queriedEntityUri));

			List<RecommendedEntity> recommendedEntities = new ArrayList<>();

			// URI is unique for all entities
			if (results != null && results.size() > 0) {
				for (SolrDocument solrDoc : results) {
					String recommendedEntityUri = (String) solrDoc
							.getFieldValue(Constants.SOLR_URI);

					List<String> allValuesOfField = null;
					Object fieldValue = solrDoc
							.getFieldValue(Constants.SOLR_RELATIONSHIP);
					if (fieldValue instanceof String) {
						allValuesOfField = Arrays.asList((String) fieldValue);
					} else if (fieldValue instanceof ArrayList) {
						allValuesOfField = (ArrayList<String>) fieldValue;
					}
					// String recommendedEntityRel = (String) solrDoc
					// .getFieldValue(Constants.SOLR_RELATIONSHIP);

					recommendedEntities.add(new RecommendedEntity(
							recommendedEntityUri, allValuesOfField));
				}
				return recommendedEntities;
			} else {
				throw new Exception("Found 0 Documents for given URI: "
						+ queriedEntityUri + " AND user: " + userName);
			}
		} catch (SolrServerException e) {
			throw new Exception("Connection to solr server at URL: "
					+ Constants.SOLR_CS_298_PROJECT_URL + " has issues.", e);
		}
	}

	private String generateQueryForProjectCollection(String userName,
			String queriedEntityUri) {
		return Constants.SOLR_URI + ":" + queriedEntityUri + " AND "
				+ Constants.SOLR_USER_ID + ":" + userName;
	}
}
