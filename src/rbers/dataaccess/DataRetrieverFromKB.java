package rbers.dataaccess;

import java.util.List;

import rbers.datatypes.Entity;
import rbers.datatypes.RecommendedEntity;
import rbers.datatypes.Relationship;

/**
 * This is a common interface for retrieving required data from Knowledge
 * Base(KB) and user profile irrespective of the database (solr or mongoDB) we
 * have taken.
 *
 * @author rakhirpv24
 *
 */
public interface DataRetrieverFromKB {

	/**
	 * This takes an entity name as input and look for it in KB. If entity not
	 * found in KB, then an exception will be thrown, else if found in KB, then
	 * it get all the relationships and the entities connected to this entity
	 * through these relationships, and creates a map of that, corresponding to
	 * this entity
	 *
	 * @param entityName
	 *            Input Entity name
	 * @return Object of Entity
	 * @throws Exception
	 */
	public Entity getNewEntity(String entityName) throws Exception;

	/**
	 * This method gets the list of relationships which are present in both
	 * queried entity and the user profile containing all the relationships the
	 * user has clicked in the past. It returns the relationship in decreasing
	 * order of click count.
	 *
	 * @param userName
	 *            Name of the user whose user profile has to be searched
	 * @param queriedEntity
	 *            An object of the entity which is queried by the user.
	 * @param startingOffset
	 *            Offset from where the data has to be compared in user profile
	 * @param numberOfRelationshipsThisQuery
	 *            Upper limit of the relationship, the algorithm would check in
	 *            user profile
	 * @return list of Relationship objects
	 * @throws Exception
	 */
	public List<Relationship> getOrderedRelationshipsForUser(String userName,
			Entity queriedEntity, int startingOffset,
			int numberOfRelationshipsThisQuery) throws Exception;

	/**
	 * Closes the connection from Solr
	 */
	public void closeConnection();

	/**
	 * This increases the click count of the relationship in user profile, if at
	 * any moment of time, user has clicked on the entity connected through that
	 * relationship
	 *
	 * @param userName
	 *            Name of the user whose user profile has to be searched
	 * @param relationship
	 *            Object of Relationship
	 * @throws Exception
	 */
	public void increaseUserRelationshipClickCount(String userName,
			List<Relationship> relationship) throws Exception;

	/**
	 * This method clears the context of the given user. This method should be
	 * called, when user logs out from,the system or clears the context manually
	 *
	 * @param userName
	 * @throws Exception
	 */
	public void clearUserContext(String userName) throws Exception;

	public List<RecommendedEntity> getRecommendedEntities(String userName,
			String queriedEntityName) throws Exception;

}
