package rbers.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.solr.client.solrj.SolrServerException;

import rbers.common.Constants;
import rbers.dataaccess.DataRetrieverFromKB;
import rbers.dataaccess.SolrJDataRetrieverFromKB;
import rbers.datatypes.RecommendedEntity;
import rbers.datatypes.Relationship;

/**
 * Servlet implementation class RbersServlet
 */
public class RbersServletWithSolrPlugin extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private DataRetrieverFromKB solrDataRetriever = new SolrJDataRetrieverFromKB();

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RbersServletWithSolrPlugin() {
		System.out.println("Into Servlet constructor");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	/*
	 * Sets the status code of attribute: relatedEntitySearchCode (200 when
	 * Entity is found, 404 when Entity is not found)
	 */
	@Override
	protected void doGet(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		String userName = request.getParameter("userName");
		long startTime = System.currentTimeMillis();
		if (request.getParameter("clearContextButton") != null) {
			clearCurrentContext(request, userName);
		}

		if (request.getParameter("searchEntityButton") != null) {
			recommendRelatedEntities(request, userName);
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Total Running time for user: " + userName
				+ " for entity " + request.getParameter("entityName")
				+ " is : " + (endTime - startTime)
				+ " ms for webapp rberswithsolrplugin");
		request.getRequestDispatcher("index-with-solr-plugin.jsp").forward(
				request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	private void clearCurrentContext(HttpServletRequest request, String userName) {
		// clear context button was clicked
		try {
			solrDataRetriever.clearUserContext(userName);
		} catch (Exception e) {
			if (e instanceof SolrServerException || e instanceof IOException) {
				request.setAttribute("relatedEntitySearchCode", "503");
			}
		}
	}

	private void recommendRelatedEntities(HttpServletRequest request,
			String userName) {
		String entityName = request.getParameter("entityName");

		// Appending DBPedia URI to the entity name given by user
		// if (entityName.contains(Constants.DBPEDIA_URI_PREFIX) == false) {
		if (entityName.contains("http") == false) {
			entityName = "\"" + Constants.DBPEDIA_URI_PREFIX
					+ entityName.replace(' ', '_') + "\"";
		}
		// else {
		// entityName = "\"" + entityName + "\"";
		// }
		try {
			List<RecommendedEntity> recommendedEntities = solrDataRetriever
					.getRecommendedEntities(userName, entityName);
			request.setAttribute("relatedEntitySearchCode", "200");
			request.setAttribute("recommendedEntities", recommendedEntities);
		} catch (Exception e) {
			if (e.getMessage().contains("Found 0 Documents")) {
				request.setAttribute("relatedEntitySearchCode", "404");
			} else if (e.getMessage().contains("Connection to solr server")) {
				request.setAttribute("relatedEntitySearchCode", "503");
			} else {
				request.setAttribute("relatedEntitySearchCode", "500");
			}
		}
		String clickedRelationshipName = request
				.getParameter("relationshipName");
		if (clickedRelationshipName != null) {
			// TODO: Uncomment the following code before submission. This is
			// commented only for testing.
			// We called this doGet method by clicking a URL to this servlet
			try {
				List<Relationship> relationships = new ArrayList<>();
				String[] clickedRelationshipNames = clickedRelationshipName
						.split(",");
				for (String s : clickedRelationshipNames) {
					relationships.add(new Relationship(s));
				}
				solrDataRetriever.increaseUserRelationshipClickCount(userName,
						relationships);
			} catch (Exception e) {
				// TODO: If 404 is already set, then is it better to show
				// 503,
				// or 404
				request.setAttribute("relatedEntitySearchCode", "503");
				request.setAttribute("orderedRelationshipNames", null);
				request.setAttribute("resultRelationshipAndEntitiesMap", null);
			}
		}
	}
}
