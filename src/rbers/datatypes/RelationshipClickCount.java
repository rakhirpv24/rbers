package rbers.datatypes;

/**
 * This represents a relationship and its corresponding click count in user
 * profile
 *
 * @author rakhirpv24
 *
 */
public class RelationshipClickCount {
	private Relationship relationship;
	private long clickCount;

	/**
	 * Constructor
	 *
	 * @param relationship
	 *            Object of Relationship
	 * @param clickCount
	 *            Number of times, user has clicked on an entity connected
	 *            through this relationship
	 */
	public RelationshipClickCount(Relationship relationship, long clickCount) {
		this.relationship = relationship;
		this.clickCount = clickCount;
	}

	/**
	 * This gets the relationship
	 *
	 * @return Object of Relationship
	 */
	public Relationship getRelationship() {
		return relationship;
	}

	/**
	 * This gets the click count of the relationship
	 *
	 * @return click count of the relationship
	 */
	public long getClickCount() {
		return clickCount;
	}

	/**
	 * This sets the click count of the relationship
	 * 
	 * @param clickCount
	 *            Number of times, user has clicked on an entity connected
	 *            through this relationship
	 */
	public void setClickCount(long clickCount) {
		this.clickCount = clickCount;
	}
}
