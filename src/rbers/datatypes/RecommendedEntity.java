package rbers.datatypes;

import java.util.List;

public class RecommendedEntity {
	private String uri;
	private List<String> relationshipName;

	public RecommendedEntity(String uri, List<String> relationshipName) {
		this.uri = uri;
		this.relationshipName = relationshipName;
	}

	public String getUri() {
		return uri;
	}

	public List<String> getRelationshipName() {
		return relationshipName;
	}

}
