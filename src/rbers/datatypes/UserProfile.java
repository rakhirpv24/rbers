package rbers.datatypes;

/**
 * This represents the user profile and will have the information of the serach
 * history for all users. Every user will be distinguished from every other user
 * through the user name. User name should be unique
 *
 * @author rakhirpv24
 *
 */
public class UserProfile {

	private String userName;

	/**
	 * Constructor
	 *
	 * @param userName
	 *            Name of the user
	 */
	public UserProfile(String userName) {
		this.userName = userName;
	}

	/**
	 * This gets the user name
	 * 
	 * @return user name
	 */
	public String getUserName() {
		return userName;
	}
}
