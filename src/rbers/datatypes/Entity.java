package rbers.datatypes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class represents an entity in the dataset. This can be visualized as a
 * <doc> in Solr terminology. Every entity has a unique ID and would be
 * connected to other entities through some relationships
 */
public class Entity {

	private Map<Relationship, List<Entity>> relationshipAndEntityMap = new HashMap<>();
	private String uniqueIdentifier;

	/**
	 * Constructor
	 *
	 * @param uniqueIdentifier
	 *            Unique ID for the entity
	 */
	public Entity(String uniqueIdentifier) {
		this.uniqueIdentifier = uniqueIdentifier;
	}

	/**
	 * This creates a map of relationship and the entities connected to this
	 * entity through this relationship
	 *
	 * @param relationship
	 *            Object of Relationship
	 * @param entities
	 *            List of Entity
	 */
	public void addRelationshipAndEntity(Relationship relationship,
			List<Entity> entities) {
		if (relationshipAndEntityMap.containsKey(relationship)) {
			List<Entity> currentEntities = relationshipAndEntityMap
					.get(relationship);
			if (currentEntities == null) {
				relationshipAndEntityMap.put(relationship, entities);
			} else {
				currentEntities.addAll(entities);
			}
		} else {
			relationshipAndEntityMap.put(relationship, entities);
		}
	}

	/**
	 * This gets the map of relationship and the entities connected to this
	 * entity through that relationship
	 *
	 * @return map of relationship and the entities connected to this entity
	 *         through that relationship
	 */
	public Map<Relationship, List<Entity>> getRelationshipAndEntityMap() {
		return relationshipAndEntityMap;
	}

	/**
	 * This gets the unique ID of this entity
	 *
	 * @return unique identifier of this entity
	 */
	public String getUniqueIdentifier() {
		return uniqueIdentifier;
	}

	@Override
	public String toString() {
		StringBuffer sb = new StringBuffer("");
		sb.append("Entity: \n\tUniqueIdentifier: ").append(uniqueIdentifier)
				.append(",");
		for (Relationship rel : relationshipAndEntityMap.keySet()) {
			sb.append(rel.getName()).append("->").append("[");
			for (Entity entity : relationshipAndEntityMap.get(rel)) {
				sb.append(entity.getUniqueIdentifier()).append(",");
			}
			sb.append("]").append(",");
		}
		return sb.substring(0, sb.length() - 1);
	}
}
