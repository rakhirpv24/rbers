<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<%@ page import="java.util.*"%>
<%@ page import="rbers.datatypes.*"%>
<%@ page import="rbers.common.*"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<script type="text/javascript">

    function stopReloadKey(evt) {
      var evt = (evt) ? evt : ((event) ? event : null);
      var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
      if (evt.keyCode == 13)  {
          return false;
      }
    }

    document.onkeypress = stopReloadKey;

    </script> 
	<%!String getEntityNameFromUri(String uri) {
		if (uri.contains("http://")) {
			int lastIndexOfString = uri.length();
			if (uri.charAt(uri.length() - 1) == '"') {
				lastIndexOfString = uri.length() - 1;
			}
			uri = uri.substring(uri.lastIndexOf("/") + 1, lastIndexOfString);
		}
		uri = uri.replace("_", " ");
		return uri;
	}%>
	<form action="RbersServletWithSolrPlugin"  defaultbutton="searchEntityButton">
		<table>
			<tr><td><input type="hidden" name="searchEntityButton"
					value="Search Entity"/></td></tr>
			<tr>
				<td>User Name <input type="text" name="userName"
					value=<%=request.getParameter("userName") != null ? request
					.getParameter("userName") : ""%> />
				</td>
				
				<td align = "left"><input type="submit" name="clearContextButton"
					value="Clear Context" /></td>
					
			</tr>
			<tr></tr>
			<tr></tr>
			<tr>
				<%
					String currentEntityName = request.getParameter("entityName") != null ? request
							.getParameter("entityName") : "";
					if (currentEntityName.contains(Constants.DBPEDIA_URI_PREFIX)) {
						currentEntityName = currentEntityName.substring(
								currentEntityName.lastIndexOf('/') + 1,
								currentEntityName.length() - 1);
					}
				%>
				<td>Enter Entity Name <input type="text" name="entityName"
					value="<%=currentEntityName%>" /></td>
			</tr>
			<tr>
				<td><input type="submit" name="searchEntityButton"
					value="Search Entity" /></td>
				
			</tr>
			<tr></tr>
			<tr></tr>
			<tr></tr>
			<tr></tr>
			<tr></tr>
			<tr></tr>
			<tr></tr>
			<tr></tr>
			<tr></tr>
			<tr></tr>
			<tr></tr>
			<tr></tr>
			<tr></tr>
			<%
				Object relatedEntitySearchCodeObj = (String) request
						.getAttribute("relatedEntitySearchCode");
				if (relatedEntitySearchCodeObj != null) {
					String relatedEntitySearchCode = (String) relatedEntitySearchCodeObj;
					if (relatedEntitySearchCode.equals("404")) {
			%>
			<tr>
				<td>Entity <%
					out.print(currentEntityName);
				%> not found.
				</td>
			</tr>
			<%
				} else if (relatedEntitySearchCode.equals("200")) {
						List<RecommendedEntity> recommendedEntities = (ArrayList<RecommendedEntity>) request
								.getAttribute("recommendedEntities");

						for (RecommendedEntity recommendedEntity : recommendedEntities) {
							String uri = recommendedEntity.getUri();
							if (uri.contains("'")) {
								// Replace single quote with ASCII code to fix URI breaking issue
								uri = uri.replace("'", "%27");
							}
							StringBuffer sb = new StringBuffer();
							for (int i = 0; i < recommendedEntity
									.getRelationshipName().size(); i++) {
								sb.append(recommendedEntity.getRelationshipName()
										.get(i));
								if (i < recommendedEntity.getRelationshipName()
										.size() - 1) {
									sb.append(",");
								}
							}
							String relNames = sb.toString();
			%><tr>
				<td><a
					href='RbersServletWithSolrPlugin?userName=<%=request.getParameter("userName")%>&entityName=<%="\"" + uri + "\""%>&relationshipName=<%=relNames%>&searchEntityButton=Search Entity'>
						<%
							out.print(getEntityNameFromUri(uri));
						%>
				</a> <%
 	out.print(" -> (" + relNames.replace(",", ", ") + ")");
 %></td>
			</tr>
			<%
				}
					} else if (relatedEntitySearchCode.equals("503")) {
			%>
			<tr>
				<td>Unable to update User profile due to some error in server.
				</td>
			</tr>
			<%
				}
				}
			%>
		</table>
	</form>
</body>
</html>